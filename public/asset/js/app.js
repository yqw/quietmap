$(document).ready(function(){ init(); initVideo(); initMarkPoint(); });

var points = [
    // {x: 350, y: 150, _id: 1, video: 'video/01.mp4'},
    // {x: 325, y: 250, _id: 2, video: 'video/02.mp4'},
    // {x: 330, y: 340, _id: 3, video: 'video/03.mp4'},
    // {x: 195, y: 405, _id: 4, video: 'video/04.mp4'}
    {x: 350, y: 150, _id: 1, video: 'http://graphics8.nytimes.com/packages/video/multimedia/bundles/projects/2013/Quiet/carl_schurz_970.mp4'},
    {x: 325, y: 250, _id: 2, video: 'http://graphics8.nytimes.com/packages/video/multimedia/bundles/projects/2013/Quiet/corona_970.mp4'},
    {x: 330, y: 340, _id: 3, video: 'http://graphics8.nytimes.com/packages/video/multimedia/bundles/projects/2013/Quiet/gantry_state_park_970.mp4'},
    {x: 195, y: 405, _id: 4, video: 'http://graphics8.nytimes.com/packages/video/multimedia/bundles/projects/2013/Quiet/inwood_hill_970.mp4'}
];

// add videos to #video-list
function init() {
    for (var i = 0, len = points.length; i < len; i++) {
        var v = $('<video id="video_' + points[i]._id + '" class="video" src="' + points[i].video + '" muted type="video/mp4"></video>');
        $('#video-list').append(v);
    }
	
	// resize windows
    $('#btn-fullscreen').click(function() {
        $('#btn-fullscreen').hide();
        activateFullscreen(document.documentElement);
    });
	
    $(window).resize(resize($('.video')));
    
	document.documentElement.addEventListener('webkitfullscreenchange', function() {
        if (!document.webkitIsFullScreen) {
            $('#btn-fullscreen').show();
        }
    });
}

// add mark point to #quiet-map
function initMarkPoint() {
    var map = $('#quiet-map'),
        mask = $('.mask'),
        p = {};

    for (var i = 0, len = points.length; i < len; i++) {
        p = $('<a id="point_' + points[i]._id + '" class="mark-point" href="javascript:;"></a>');
        p.css({
            left: points[i].x,
            top: points[i].y
        });
        map.append(p);
    }

    $('.mark-point').click(function(event) {
        if (!map.hasClass('close')) {
        	var target = $(event.target),
				vID = target.attr('id').replace('point_', ''),
				video = $('#video_' + vID),
				currentVideo = $('.video.current');
            
            map.addClass('close');
            mask.addClass('close');
			
			target.addClass('current');
			
			currentVideo[0].pause();
            currentVideo.animate({opacity: 0}, 300);
			currentVideo.removeClass('current');
            
			video.css({opacity: 1});
			video.addClass('show current');
            video.prop('muted', false);
            
			video.bind('ended', function() {
                video.prop('muted', true);
                video.animate({opacity: 0}, 0);
				// video.animate({opacity: 0}, 100);
				video.removeClass('show');
                map.removeClass('close');
                mask.removeClass('close');
				target.removeClass('current');
                playVideo();
            });
			
            video[0].play();
        }
    });
}


// load & init video
function initVideo() {
    var videos = $('.video'),
		i = 0,
		len = points.length;
	
    $('.video').bind('loadeddata', function() {
        i += 1;
        if (i >= len) {
	        resize(videos);
	        $(videos[getRandomFromRange(len)]).addClass('current');
            playVideo();
        }
    });
}

function playVideo() {
    var videos = $('.video'),
        currentVideo = $('.video.current'),
        index = videos.index(currentVideo),
        index = index + 1 >= videos.length ? 0 : index + 1,
        next = $(videos[index]);

	currentVideo[0].ontimeupdate = function() {
		
		if (!currentVideo.hasClass('show') && currentVideo[0].currentTime >= 5) {
			currentVideo.animate({'opacity': 0}, 300);
	        currentVideo.removeClass('current');
			currentVideo[0].currentTime = 0;
	        currentVideo[0].pause();
	
	        next.addClass('current');

			playVideo();
		}
	};
	
    currentVideo.animate({'opacity': 1}, 300);
    currentVideo[0].play();
}

// resize video size
function resize(videos) {
    for (var i = 0, len = videos.length; i < len; i++) {
        var ovH = videos[i].videoHeight,
            ovW = videos[i].videoWidth,
            wH = $(document).height(),
            wW = $(document).width(),
            tH = wH / ovH,
            tW = wW / ovW;
        if (tH <= tW) {
            h = ovH * tW;
            w = ovW * tW;
        } else {
            h = ovH * tH;
            w = ovW * tH;
        }
        $(videos[i]).css({
            'height': h + 'px',
            'width': w + 'px',
            'margin-left': (- w / 2) + 'px',
            'margin-top': (- h / 2) + 'px'
        }); 
    }
}

function loadVideo(player, video, callback) {
    player.bind('loadeddata', function() {
        callback();
    })
    player.attr('src', video);
    player.load();
}

function getRandomFromRange(length) {
    return Math.floor(length * Math.random());
}

function activateFullscreen(element) {
    if(element.requestFullScreen) {
        element.requestFullScreen();
    } else if(element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
    }
}
